%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Creation of the wrench %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

hmax = 0.15; % mesh size: 0.5=coarse , 0.15=fine , 0.08 = ULTRA fine
precomputeEverything = 1; % you need it to be 1 in order to compute gradients, and 2 for parallel precomputation

wrench = Wrench(hmax,precomputeEverything);


% plot the geometry & the mesh
figure(1)

subplot(2,1,1)
wrench.plotGeometry();

subplot(2,1,2)
wrench.plotMesh();

%% The parameter is the "log of YoungModulus field"
% get a realization from it (Gaussian distrib)...
logE = wrench.logE(); 
% ... and plot it
clf
wrench.plot(logE)

%% Compute and plot the solution
logE = wrench.logE();
wrench.plotSol(logE)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The observation and its gradient %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Vertical displacement of the solution on the left of the wrench
% type = 1;
% Von Mises stress at some point
% type = 2;
% Displacement along the line
type = 3;

logE = wrench.logE();
[y,g,XObs,L] = wrench.evalObs(logE,type);


% disp('Quantity of Interest')
% disp(y)
% wrench.plot(g)
% hold on
% plot(XObs(1),XObs(2),'or')
% hold off

plot(XObs(1,:),y)

%% Norm on the data space (if type=3)

RX = wrench.M + wrench.K;
[~,~,~,L] = wrench.evalObs(wrench.logE(),3);
L = wrench.B'*L;
RYinv = full(L'*(RX\L));
RY = inv(RYinv);

% Then, measure distances in Im(L) using the metric induced by 
%      <.,.> = (.^T)*RY*(.)

%% Matrix H for dimension reduction
K=500;
H = zeros(wrench.dimParam);
data = zeros(size(XObs,2),K);
for k=1:K
    logE = wrench.logE();
    [y,g] = wrench.evalObs(logE,type);
    data(:,k) = y;
%     H = H + g'*g;
    H = H + g'*RY*g;
end
H = H/K;
CovData = cov(data');


%% Compute the *informed* directions
[UU,DD] = svd( wrench.Sigma12'*H*wrench.Sigma12);
DD = diag(DD);
UU = wrench.Sigma12*UU;

subplot(2,2,1)
semilogy(DD)
for i=1:3
    subplot(2,2,i+1)
    wrench.plot(UU(:,i))
    title(['Mode ' num2str(i)])
end

%% Compute the *informative* directions
% A = chol(RYinv);
A = chol(RY)
[UU,DD] = svd(A*CovData*A');
DD = diag(DD);
UU = A\UU;
subplot(1,2,1)
semilogy(DD)
subplot(1,2,2)
plot(XObs(1,:),UU(:,1:6))
legend(arrayfun(@(x) ['Mode ' num2str(x)],1:4,'UniformOutput',0))

%%%%%%%%%%%%%%%%%%%%
%% The multilevel %%
%%%%%%%%%%%%%%%%%%%%

wrench_coarse = Wrench(0.22);
wrench_fine = Wrench(0.1);

clf

subplot(2,1,1)
wrench_coarse.plotMesh()

subplot(2,1,2)
wrench_fine.plotMesh()


%% fine2coarse matrix : 
% we'll need that to compute the "coarse parameter" as a function of the
% "fine paremeter"
f2c = fine2coarse(wrench_fine.PDEmodel,wrench_coarse.PDEmodel);

%% Playing with the two models (coarse <-> fine)

% draw the parameter (fine scale)
logEf =  wrench_fine.logE();
% compute the coarse parameter ("coarse = f2c * fine")
logEc = f2c*logEf;

clf

subplot(2,1,1)
wrench_fine.plot(logEf)
shading interp

subplot(2,1,2)
wrench_coarse.plot(logEc)
shading interp

%% the solutions....
% new parameter...
logEf =  wrench_fine.logE();
logEc = f2c*logEf;


clf

subplot(2,1,1)
wrench_fine.plotSol(logEf)
shading interp

subplot(2,1,2)
wrench_coarse.plotSol(logEc)
shading interp


%% the gradients (of the QoI)
% new parameter...
logEf =  wrench_fine.logE();
logEc = f2c*logEf;

% fine scale resolution
tic;
[yf,gf] = evalObs(wrench_fine,logEf);
time1=toc;

% coarse scale resolution
tic;
[yc,gc] = evalObs(wrench_coarse,logEc);
time2=toc;

disp('-----------------')
disp('Time ratio')
disp(time2/time1)

% error in the gradient approximation
gc = f2c'*gc;
disp('Relative error ')
disp( norm(gf-gc)/norm(gf) )

% plot...
subplot(2,1,1)
wrench_fine.plot(gf)
shading interp

subplot(2,1,2)
wrench_fine.plot(gc)
shading interp




