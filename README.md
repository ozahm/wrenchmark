# Wrenchmark


The paper can be found on arxiv, in case it’s not already on the website:
https://arxiv.org/abs/1809.05567
the bibtex:

@article{lam2018multifidelity,
  title={Multifidelity Dimension Reduction via Active Subspaces},
  author={Lam, R{\'e}mi and Zahm, Olivier and Marzouk, Youssef and Willcox, Karen},
  journal={arXiv preprint arXiv:1809.05567},
  year={2018}
}
