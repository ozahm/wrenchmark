function fine2coarse = fine2coarse(PDEmodel_fine,PDEmodel_coarse)


% Get the center of the elements
Tf=PDEmodel_fine.Mesh.Elements;
Pf=PDEmodel_fine.Mesh.Nodes;
coordf=[mean( reshape(Pf(1,Tf(:)),size(Tf)) ) ; mean( reshape(Pf(2,Tf(:)),size(Tf)) )];
areaf=polyarea(reshape(Pf(2,Tf(:)),size(Tf)),reshape(Pf(1,Tf(:)),size(Tf)));

Tc=PDEmodel_coarse.Mesh.Elements;
Pc=PDEmodel_coarse.Mesh.Nodes;
coordc=[mean( reshape(Pc(1,Tc(:)),size(Tc)) ) ; mean( reshape(Pc(2,Tc(:)),size(Tc)) )];



ne_fine = size(PDEmodel_fine.Mesh.Elements,2);
ne_coarse = size(PDEmodel_coarse.Mesh.Elements,2);
fine2coarse = sparse(ne_coarse,ne_fine);

for k=1:ne_coarse
    
    x1 = PDEmodel_coarse.Mesh.Nodes(:,PDEmodel_coarse.Mesh.Elements(1,k));
    x2 = PDEmodel_coarse.Mesh.Nodes(:,PDEmodel_coarse.Mesh.Elements(2,k));
    x3 = PDEmodel_coarse.Mesh.Nodes(:,PDEmodel_coarse.Mesh.Elements(3,k));
    
    IND=[];
    IND(1,:) = sign( (x2-x1)'*[0 -1;1 0]*(coordf-x1) ) * sign( (x2-x1)'*[0 -1;1 0]*(x3-x1) );
    IND(2,:) = sign( (x3-x2)'*[0 -1;1 0]*(coordf-x2) ) * sign( (x3-x2)'*[0 -1;1 0]*(x1-x2) );
    IND(3,:) = sign( (x1-x3)'*[0 -1;1 0]*(coordf-x3) ) * sign( (x1-x3)'*[0 -1;1 0]*(x2-x3) );
    IND = find(prod(IND>0));
    
    fine2coarse(k,IND)=areaf(IND);
    
end

% Check that each coarse element has (at least) one fine element inside...
if ~isempty( find(sum(fine2coarse,2)==0, 1) )
    error('At least one coarse element doesnt''t have any fine element inside...')
end

% All fine element which are left alone are attached to the closest coarse element
leftAloneElements = find(sum(fine2coarse)==0);
for i=1:length(leftAloneElements)
    IND = coordc - coordf(:,leftAloneElements(i));
    IND = sum(IND.^2);
    [~,IND] = min(IND);
    fine2coarse(IND,leftAloneElements(i)) = areaf(leftAloneElements(i));
end

fine2coarse = spdiags(1./sum(fine2coarse,2) ,0,ne_coarse,ne_coarse )*fine2coarse;
